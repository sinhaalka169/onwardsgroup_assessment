import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor( private httpClient: HttpClient) { }

  getdata(x) {
    return this.httpClient.get(`https://vpic.nhtsa.dot.gov/api/vehicles/GetMakeForManufacturer/${x}?format=json`);
  }
}
