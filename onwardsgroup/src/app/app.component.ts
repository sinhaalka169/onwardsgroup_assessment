import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dataval:any = {
    Count: '',
    Message: '',
    Results: ''
  };
  x = ''
  constructor( private appService:AppService) {
    
  }

  getDataval() {
    console.log(this.x);
    this.appService.getdata(this.x).subscribe(resp => {
      this.dataval = resp;
      console.log(this.dataval.SearchCriteria, 'alka');
    })
  }

  clearbtn() {
    this.dataval = '';
  }
}
